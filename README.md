# @HappyTiptoe/clbi Project

This template is intended to kickstart your development with Vue 3 and Vite. It follows an opinionated project-structure, not too dissimilar to that of [Nuxt](https://nuxt.com)!

## Setup Project

### Install dependencies

```sh
# yarn
yarn install

# pnpm
pnpm install

# npm
npm install
```

### Launch the development server:

Start the development server on `http://localhost:8888`:

```sh
# yarn
yarn dev

# pnpm
pnpm run dev

# npm
npm run dev
```

Or use a custom port:

```sh
# yarn
yarn dev --port 1234

# pnpm
pnpm run dev --port 1234

# npm
npm run dev --port 1234
```

### Build the application for production:

```sh
# yarn
yarn build

# pnpm
pnpm run build

# npm
npm run build
```

### Locally preview the production build:

```sh
# yarn
yarn preview

# pnpm
pnpm run preview

# npm
npm run preview
```

## Customise Configuration

See [Vite Configuration Reference](https://vitejs.dev/config/)

## Included Tooling + Documentation

- [Pinia](https://pinia.vuejs.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [tsu](https://gitlab.com/happytiptoe/tsu/)
- [TypeScript](https://www.typescriptlang.org/)
- [unplugin-auto-import](https://github.com/antfu/unplugin-auto-import/)
- [unplugin-vue-components](https://github.com/antfu/unplugin-vue-components/)
- [Vite](https://vitejs.dev/)
- [vite-plugin-pages](https://github.com/hannoeru/vite-plugin-pages)
- [Vue](https://vuejs.org/)
- [VueUse](https://vueuse.org/)
- [Vue Router](https://router.vuejs.org/)
