const dT = require('tailwindcss/defaultTheme')

/** @type { import('tailwindcss').Config } */
module.exports = {
  content: ['./src/**/*.{html,js,ts,vue}'],
  // important: '#app',
  darkMode: 'media', // or 'class'
  theme: {
    container: {
      center: true,
      padding: '1rem'
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px'
    },
    extend: {
      fontFamily: {
        sans: ['"Inter Variable"', ...dT.fontFamily.sans],
        display: ['"Grandstander Variable"', ...dT.fontFamily.sans]
      }
    }
  }
}
