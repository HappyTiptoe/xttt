import { range } from 'tsu'
import { Grid, GridCell, GridState, Player } from '~/types'

export function createGrids() {
  return range(9).map(() => ({
    cells: range(9).map(() => GridCell.EMPTY),
    state: GridState.IN_PROGRESS,
    winner: null
  }))
}

/* * */

export function setGridCell(grid: Grid, cellIdx: number, currentPlayer: Player) {
  const cell = currentPlayer === Player.X ? GridCell.X : GridCell.O

  grid.cells[cellIdx] = cell
}

/* * */

const WINNING_COMBINATIONS = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [6, 4, 2]
]

export function getGridState(grid: Grid) {
  const cells = grid.cells

  for (const combo of WINNING_COMBINATIONS) {
    const x = cells[combo[0]]
    const y = cells[combo[1]]
    const z = cells[combo[2]]

    if (x === y && y === z && x !== GridCell.EMPTY) {
      return GridState.WIN
    }
  }

  if (cells.every((c) => c !== GridCell.EMPTY)) {
    return GridState.TIE
  }

  return GridState.IN_PROGRESS
}

/* * */

export function updateGridState(grid: Grid, currentPlayer: Player) {
  const state = getGridState(grid)

  grid.state = state

  if (state === GridState.WIN) {
    grid.winner = currentPlayer
  }
}

/* * */

export function gameOverCheck(grids: Grid[]) {
  return grids.every((grid) => grid.state !== GridState.IN_PROGRESS)
}

/* * */

export function getGameWinner(grids: Grid[]) {
  let xWins = 0
  let oWins = 0

  for (const grid of grids) {
    if (grid.winner === Player.X) {
      xWins++
    } else if (grid.winner === Player.O) {
      oWins++
    }
  }

  if (xWins > oWins) {
    return Player.X
  } else if (xWins < oWins) {
    return Player.O
  } else {
    return null
  }
}
