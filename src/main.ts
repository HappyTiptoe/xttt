import { persistia } from 'persistia'
import { createPinia } from 'pinia'
import { createApp } from 'vue'
import App from './App.vue'

import '@fontsource-variable/grandstander'
import '@fontsource-variable/inter'
import './css/tailwind.css'

const app = createApp(App)

/* pinia */
const pinia = createPinia()
pinia.use(persistia)
app.use(pinia)

app.mount('#app')
