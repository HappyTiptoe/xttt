export enum Player {
  X = 'X',
  O = 'O'
}

export enum GridCell {
  EMPTY = ' ',
  X = 'X',
  O = 'O'
}

export enum GridState {
  IN_PROGRESS,
  TIE,
  WIN
}

export interface Grid {
  cells: GridCell[]
  state: GridState
  winner: Player | null
}
