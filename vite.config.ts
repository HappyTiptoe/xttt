import vue from '@vitejs/plugin-vue'
import autoImport from 'unplugin-auto-import/vite'
import components from 'unplugin-vue-components/vite'
import pages from 'vite-plugin-pages'
import { fileURLToPath, URL } from 'url'
import { defineConfig } from 'vite'

export default defineConfig({
  root: './src',
  base: './',
  resolve: {
    alias: {
      '~': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  plugins: [
    autoImport({
      imports: ['vue', 'vue-router', '@vueuse/core'],
      dirs: ['composables', 'stores']
    }),
    components({ dirs: ['components'] }),
    // pages({ dirs: 'pages' }),
    vue()
  ],
  server: {
    port: 8888,
    strictPort: true,
    fs: {
      strict: false
    }
  },
  build: {
    outDir: '../dist',
    rollupOptions: {
      output: {
        chunkFileNames: 'assets/[name]-[hash].js'
      }
    },
    minify: 'esbuild' // or `false`
  }
})
